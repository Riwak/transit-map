# README #

Ou est le prochain Métro ? Transit Map est un outil de prédiction en temps réel adapté au réseaux métro/RER de la région Parisienne. L'outil se base sur les données Open Data fournie par la RATP en Open source ainsi que sur une solution de modélisation en javascript des différentes lignes de transports. 

Il est désormais possible de suivre son métro, visualiser sa ligne et observer l'ensemble du trafic parisien. Il s'agit d'une version Bêta en cours de développement. Les retard ne sont pas gérés. 
De même, la disponibilité et l'intégrité des données n'est pas garantie : suppressions de rames, retards, défaillances sur l'API RATP ne sont pas pris en charge.

### Contribution ###

* Audouin d'Aboville
* Valentin Boistel-Deniset

### Version ###
* V 0.1
* Codé en PHP/ JS/ Gtfs-data
* BDD en GTFS-Data et MysQL

### Infos ###

EFREI : Projet Transport - M1 - 2016